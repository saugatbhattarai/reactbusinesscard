import IntroComponent from "./components/IntroComponent";
import ButtonComponent from "./components/ButtonComponent";
import MainComponent from "./components/MainComponent";
import FooterComponent from "./components/FooterComponent";

export default function App() {
  return (
    <div className="container">
        <IntroComponent />
        <ButtonComponent />
        <MainComponent />
        <FooterComponent />
    </div>
  );
}

