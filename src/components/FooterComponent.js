export default function FooterComponent() {
  return (
    <div className="footer--main">
      <a href="https://twitter.com/TheSaggi" target="blank">
        <i class="fa-brands fa-square-x-twitter"></i>
      </a>
      <a href="https://www.linkedin.com/in/saugatbhattarai/" target="blank">
        <i class="fa-brands fa-linkedin"></i>
      </a>
      <a href="https://www.facebook.com/SaugatBhattarai007/" target="blank">
        <i class="fa-brands fa-square-facebook"></i>
      </a>
      <a href="https://www.instagram.com/thesaggi/" target="blank">
        <i class="fa-brands fa-square-instagram"></i>
      </a>
      <a href="https://gitlab.com/saugatbhattarai" target="blank">
        <i class="fa-brands fa-square-gitlab"></i>
      </a>
    </div>
  );
}
