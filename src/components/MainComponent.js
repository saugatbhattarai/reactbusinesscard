export default function MainComponent() {
  return (
    <div className="main--content">
      <div className="main--about">
        <h3 className="content-title">- About -</h3>
        <p className="content-description">
        I'm a passionate web developer and a machine learning enthusiast
         with over 7 years of professional expertise. 
         I've contributed to the success of multiple companies by 
         creating award winning products. 
        At present, I'm pursuing my career as an independent freelancer.
        </p>
      </div>
      <div className="main--interests">
        <h3 className="content-title"> - Skills -</h3>
        <p className="content-description">
          HTML . CSS . JS . ReactJS . NodeJS .
          Python . Tornado . Flask . PHP . WordPress .
          MySQL . PostgreSQL . MongoDB  .
          Linux . Git . Figma . AWS EC2 . Docker .
          Computer Vision . Pytorch . Tensorflow  
         
        </p>
      </div>
    </div>
  );
}
