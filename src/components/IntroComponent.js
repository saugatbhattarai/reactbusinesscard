export default function IntroComponent() {
    return(
        <div className="intro--main">
            <div className="intro-image">
                <img src="./images/pp_saugat.png" alt="Saugat Bhattarai"/>
            </div>
            <div className="intro--content">
                <h2>Saugat Bhattarai</h2>
                <h5>Frontend Developer</h5>
                <a href="https://saugatbhattarai.com.np/" target="blank"><p>https://saugatbhattarai.com.np</p></a>
            </div>
        </div>
    );
}