export default function ButtonComponent() {
  return (
    <div className="button--main">
      <a href="mailto:bhattarai.saugat11@gmail.com"><button className="email-button">
        <i class="fa-solid fa-envelope"></i>
        <span>Email</span>
      </button></a>
      <a href="https://www.linkedin.com/in/saugatbhattarai/" target="blank">
        <button className="linkedin-button">
          <i class="fa-brands fa-linkedin"></i>
          <span>Linkedin</span>
        </button></a>
    </div>
  );
}